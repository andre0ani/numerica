<?php get_template_part('template-parts/header'); ?>
<!-- single -->
<div class="affichageF">
    <div class="pure-g">
        <div class="pure-u-1 pure-u-md-3-4 pure-u-lg-3-4 column-flex blog-post">
            <!-- call content-single -->
            <?php
            if (have_posts()) : while (have_posts()) : the_post();
                    get_template_part('content-single', get_post_format());

                    if (comments_open() || get_comments_number()) :
                        comments_template();
                    endif;

                endwhile;
            endif;
            ?>

        </div>
        
        <!-- call sidebar -->
        <div class="pure-u-1 pure-u-md-1-4 pure-u-lg-1 column">
            <?php get_sidebar(); ?>
        </div>
    </div>
</div>

<?php get_template_part('template-parts/footer'); ?>