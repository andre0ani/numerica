=== Numerica ===
Contributors: Patrice Andreani
Requires at least: 6.1
Tested up to: 6.4
Requires PHP: 7.4
Stable tag: 2.0
License: GPLv3
License URI: https://www.gnu.org/licenses/gpl-3.0.txt

== Description ==

Thème simple et léger pour WordPress.

== Copyright ==

Numerica Theme, (C) 2023 Patrice Andreani <andreani.patrice@net-c.fr>
Numerica is distributed under the terms of the GNU GPL.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

== Changelog ==

= 2.0 =
* Released: December 27, 2023