<!-- Error page -->
<?php get_header(); ?>
    <div class="pure-g">

        <div class="pure-u-3-4 column" id="error">
           
                <h2><?php esc_html_e('Oups... Page not found...', 'numerica') ?></h2>
                <br>
                <br>
                <?php
                echo esc_html_e('There is an error, the requested page have not been found.', 'numerica') . '<br>';
                ?>
                <br>
                <a href="<?php echo esc_url(home_url('/')); ?>"><?php esc_html_e('Back To Home', 'numerica'); ?></a>
           
        </div>

        <div class="pure-u-1-4 column">
            <?php get_sidebar(); ?>
        </div>

    </div>

<?php get_footer(); ?>