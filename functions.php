<?php
/**
 * Numerica functions.php file
 *
 * Author:          Patrice Andreani <andreani.patrice@net-c.fr>
 * Created on:      23/12/2023
 *
 */

define( 'NUMERICA_VERSION', '2.0');
define( 'NUMERICA_MAIN_DIR', get_template_directory() . '/' );
define( 'NUMERICA_BASENAME', basename( NUMERICA_MAIN_DIR ) );

if (!function_exists('numerica_block_styles')) :

  // Register custom block styles
  function numerica_block_styles()
  {

    register_block_style(
      'core/details',
      array(
        'name'         => 'arrow-icon-details',
        'label'        => __('Arrow icon', 'numerica'),
        'inline_style' => '
				.is-style-arrow-icon-details {
					padding-top: var(--wp--preset--spacing--10);
					padding-bottom: var(--wp--preset--spacing--10);
				}

				.is-style-arrow-icon-details summary {
					list-style-type: "\2193\00a0\00a0\00a0";
				}

				.is-style-arrow-icon-details[open]>summary {
					list-style-type: "\2192\00a0\00a0\00a0";
				}',
      )
    );
    register_block_style(
      'core/post-terms',
      array(
        'name'         => 'pill',
        'label'        => __('Pill', 'numerica'),
        'inline_style' => '
				.is-style-pill a,
				.is-style-pill span:not([class], [data-rich-text-placeholder]) {
					display: inline-block;
					background-color: var(--wp--preset--color--base-2);
					padding: 0.375rem 0.875rem;
					border-radius: var(--wp--preset--spacing--20);
				}

				.is-style-pill a:hover {
					background-color: var(--wp--preset--color--contrast-3);
				}',
      )
    );
    register_block_style(
      'core/list',
      array(
        'name'         => 'checkmark-list',
        'label'        => __('Checkmark', 'numerica'),
        'inline_style' => '
				ul.is-style-checkmark-list {
					list-style-type: "\2713";
				}

				ul.is-style-checkmark-list li {
					padding-inline-start: 1ch;
				}',
      )
    );
    register_block_style(
      'core/navigation-link',
      array(
        'name'         => 'arrow-link',
        'label'        => __('With arrow', 'numerica'),
        'inline_style' => '
				.is-style-arrow-link .wp-block-navigation-item__label:after {
					content: "\2197";
					padding-inline-start: 0.25rem;
					vertical-align: middle;
					text-decoration: none;
					display: inline-block;
				}',
      )
    );
    register_block_style(
      'core/heading',
      array(
        'name'         => 'asterisk',
        'label'        => __('With asterisk', 'numerica'),
        'inline_style' => "
				.is-style-asterisk:before {
					content: '';
					width: 1.5rem;
					height: 3rem;
					background: var(--wp--preset--color--contrast-2, currentColor);
					clip-path: path('M11.93.684v8.039l5.633-5.633 1.216 1.23-5.66 5.66h8.04v1.737H13.2l5.701 5.701-1.23 1.23-5.742-5.742V21h-1.737v-8.094l-5.77 5.77-1.23-1.217 5.743-5.742H.842V9.98h8.162l-5.701-5.7 1.23-1.231 5.66 5.66V.684h1.737Z');
					display: block;
				}

				.is-style-asterisk:empty:before {
					content: none;
				}

				.is-style-asterisk:-moz-only-whitespace:before {
					content: none;
				}

				.is-style-asterisk.has-text-align-center:before {
					margin: 0 auto;
				}

				.is-style-asterisk.has-text-align-right:before {
					margin-left: auto;
				}

				.rtl .is-style-asterisk.has-text-align-left:before {
					margin-right: auto;
				}",
      )
    );
  }
endif;
add_action('init', 'numerica_block_styles');



// Sets up theme defaults and registers support for various WordPress features.

function numerica_setup()
{

  // Make theme available for translation.
  load_theme_textdomain('numerica', get_template_directory() . '/languages');

  // add post format support
  add_theme_support('post-formats', array('aside', 'gallery', 'chat', 'image', 'link', 'quote', 'video', 'audio'));

  // Add default posts and comments RSS feed links to head.
  add_theme_support('automatic-feed-links');

  // Let WordPress manage the document title.
  add_theme_support('title-tag');

  // Enable support for Post Thumbnails.
  add_theme_support('post-thumbnails');

  // Admin editor styles.
  add_theme_support('editor-styles');

  // Switch default core markup for different forms to output valid HTML5.
  add_theme_support('html5', array('comment-form', 'comment-list'));

  // Add support for responsive embeds.
  add_theme_support('responsive-embeds');

  // Add theme support for selective refresh for widgets.
  add_theme_support('customize-selective-refresh-widgets');

  // Enable block styles.
  add_theme_support('wp-block-styles');

  // Enqueue editor styles.
  add_editor_style();

  // add color palette in editor
  add_theme_support(
    'editor-color-palette',
    array(
      array('name' => 'blue', 'slug'  => 'blue', 'color' => '#48ADD8'),
      array('name' => 'pink', 'slug'  => 'pink', 'color' => '#FF2952'),
      array('name' => 'green', 'slug'  => 'green', 'color' => '#83BD71'),
    )
  );

  // custom background
  $args = array(
    'default-color' => '000000',
    'default-image' => '',
  );
  add_theme_support('custom-background', $args);

  // custom font size
  add_theme_support('editor-font-sizes', array(
    array(
      'name' => esc_attr__('Small', 'numerica'),
      'size' => 12,
      'slug' => 'small'
    ),
    array(
      'name' => esc_attr__('Regular', 'numerica'),
      'size' => 16,
      'slug' => 'regular'
    ),
    array(
      'name' => esc_attr__('Large', 'numerica'),
      'size' => 36,
      'slug' => 'large'
    ),
    array(
      'name' => esc_attr__('Huge', 'numerica'),
      'size' => 50,
      'slug' => 'huge'
    )
  ));

// custom logo
  add_theme_support('custom-logo', array(
    'flex-height' => true,
    'flex-width'  => true,
    'width'       => 220,
    'height'      => 95,
    'header-text' => array('site-title', 'site-description'),
  ));

  // add navigation
  register_nav_menus(
    array(
      'primary' => __('Primary menu', 'numerica'),
    )
  );
}
add_action('after_setup_theme', 'numerica_setup');

// add media query for responsive
function numerica_get_media_query( $name ) {
	$desktop = apply_filters( 'numerica_desktop_media_query', '(min-width:1025px)' );
	$tablet_only = apply_filters( 'numerica_tablet_media_query', '(min-width: 769px) and (max-width: 1024px)' );
	$mobile = apply_filters( 'numerica_mobile_media_query', '(max-width:768px)' );
	$mobile_menu = apply_filters( 'numerica_mobile_menu_media_query', $mobile );

	$queries = apply_filters('numerica_media_queries', array(
			'desktop' => $desktop,
			'tablet_only' => $tablet_only,
			'tablet' => '(max-width: 1024px)',
			'mobile' => $mobile,
			'mobile-menu' => $mobile_menu,
		)
	);
	return $queries[ $name ];
}


final class NUMERICA_Theme_Class {

	public function __construct() {

		// Add an X-UA-Compatible header.
		add_filter( 'wp_headers', array( 'NUMERICA_Theme_Class', 'x_ua_compatible_headers' ) );
  }

  // add viexport in header
  public static function meta_viewport() {
    $viewport = '<meta name="viewport" content="width=device-width, initial-scale=1">';
        echo apply_filters( 'numerica_meta_viewport', $viewport );
  }
}
add_action( 'wp_head', array( 'NUMERICA_Theme_Class', 'meta_viewport' ), 1 );

// Register pattern categories
if (!function_exists('numerica_pattern_categories')) :
  function numerica_pattern_categories()
  {
    register_block_pattern_category(
      'page',
      array(
        'label'       => __('Pages', 'numerica'),
        'description' => __('A collection of full page layouts.', 'numerica'),
      )
    );
  }
endif;
add_action('init', 'numerica_pattern_categories');


// remove url fields in comments
function numerica_wpadmin_remove_comment_url($arg)
{
  $arg['url'] = '';
  return $arg;
}
add_filter('comment_form_default_fields', 'numerica_wpadmin_remove_comment_url');


// changes the order of fields in the comment form
function numerica_move_comment_field_to_bottom($fields)
{
  $comment_field = $fields['comment'];
  unset($fields['comment']);
  $fields['comment'] = $comment_field;
  return $fields;
}
add_filter('comment_form_fields', 'numerica_move_comment_field_to_bottom');


// link to read more articles
function numerica_post_link($output_filter = '')
{

  $enabled = apply_filters('numerica_post_link_enabled', '__return_true');
  if ((is_admin() && !wp_doing_ajax()) || !$enabled) {
    return $output_filter;
  }

  $read_more_text    = apply_filters('numerica_post_read_more', __('Read More &raquo;', 'numerica'));
  $read_more_classes = apply_filters('numerica_post_read_more_class', array());

  $post_link = sprintf(
    esc_html('%s'),
    '<a class="' . esc_attr(implode(' ', $read_more_classes)) . '" href="' . esc_url(get_permalink()) . '"> ' . get_the_title('<span class="screen-reader-text">', '</span>', false) . ' ' . $read_more_text . '</a>'
  );

  $output = ' &hellip; ' . $post_link;

  return apply_filters('numerica_post_link', $output, $output_filter);
}
add_filter('excerpt_more', 'numerica_post_link', 20);

// enqueue style scripts
function numerica_style_scripts()
{
  wp_enqueue_style('site', get_template_directory_uri() . '/style.css');
  wp_enqueue_style( 'pure-css', get_template_directory_uri() . '/css/pure-min.css', false, '3.0.0', 'all');
  wp_enqueue_style( 'pure-css-responsive', get_template_directory_uri() . '/css/grids-responsive-min.css', false, '3.0.0', 'all');
}
add_action('wp_enqueue_scripts', 'numerica_style_scripts');


// register widgets
function numerica_add_widget_Support()
{
  register_sidebar(array(
    'name' => 'Sidebar-1',
    'id' => 'sidebar-1',
    'before_widget' => '<div class="widgetsB">',
    'after_widget' => '</div>',
    'before_title' => '<h3>',
    'after_title' => '</h3><hr>',
  ));


  register_sidebar(array(
    'name' => 'widget-haut',
    'id' => 'widget-1',
    'before_widget' => '<div class="widgetsB">',
    'after_widget' => '</div>',
    'before_title' => '<h3>',
    'after_title' => '</h3><hr>',
  ));

  register_sidebar(array(
    'name' => 'widget-bas',
    'id' => 'widget-2',
    'before_widget' => '<div class="widgetsB">',
    'after_widget' => '</div>',
    'before_title' => '<h3>',
    'after_title' => '</h3><hr>',
  ));


  register_sidebar(array(
    'name' => 'widget-footer-1',
    'id' => 'widget-footer-1',
    'before_widget' => '<div class="widget_text widgetsB">',
    'after_widget' => '</div>',
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h>',
  ));

  register_sidebar(array(
    'name' => 'widget-footer-2',
    'id' => 'widget-footer-2',
    'before_widget' => '<div class="widget_text widgetsB">',
    'after_widget' => '</div>',
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
  ));

  register_sidebar(array(
    'name' => 'widget-footer-3',
    'id' => 'widget-footer-3',
    'before_widget' => '<div class="widget_text widgetsB">',
    'after_widget' => '</div>',
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
  ));

  register_sidebar(array(
    'name' => 'widget-footer-4',
    'id' => 'widget-footer-4',
    'before_widget' => '<div class="widget_text widgetsB">',
    'after_widget' => '</div>',
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
  ));
}
add_action('widgets_init', 'numerica_add_Widget_Support');


function numerica_add_theme_scripts()
{
  wp_enqueue_style('style', get_stylesheet_uri());
  if (is_singular() && comments_open() && get_option('thread_comments')) {
    wp_enqueue_script('comment-reply');
  }
}
add_action('wp_enqueue_scripts', 'numerica_add_theme_scripts');



// images and size
if (function_exists('add_theme_support')) {
  add_theme_support('post-thumbnails');
  add_theme_support('post-thumbnails', array('post'));
  add_theme_support('post-thumbnails', array('page'));
  set_post_thumbnail_size(600, 350);
}


add_post_type_support('page', 'excerpt');

function numerica_wrapper_start()
{
  echo '<section id = "main">';
}

function numerica_wrapper_end()
{
  echo '</ section>';
}


add_action('init', 'add_Main_Nav');


// Add [aria-haspopup] and [aria-expanded] to menu items that have children.
function numerica_nav_menu_link_attributes( $atts, $item, $args, $depth ) {

	$item_has_children = in_array( 'menu-item-has-children', $item->classes );
	if ( $item_has_children ) {
		$atts['aria-haspopup'] = 'true';
		$atts['aria-expanded'] = 'false';
	}

	return $atts;
}
add_filter( 'nav_menu_link_attributes', 'numerica_nav_menu_link_attributes', 10, 4 );


// add custom category widget
function numerica_custom_category_widget( $arg ) {
	$cat = get_theme_mod( 'exclude_post_cat' );

	if ( $cat ) {
		$cat            = array_diff( array_unique( $cat ), array( '' ) );
		$arg['exclude'] = $cat;
	}
	return $arg;
}
add_filter( 'widget_categories_args', 'numerica_custom_category_widget' );
add_filter( 'widget_categories_dropdown_args', 'numerica_custom_category_widget' );


// contact section
function numerica_contact_customize_register($wp_customize)
{
  $wp_customize->add_section('contact', array(
    'title' => __('Contact section', 'numerica'),
    'priority' => 30,
  ));

  $wp_customize->add_setting('contact_code', array(
    'default' => 'Contact section',
    'transport' => 'postMessage',
    'sanitize_callback'  => 'esc_attr',
  ));

  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    'contact_code',
    array(
      'label' => __('Enter the text for the contact section', 'numerica'),
      'section' => 'contact',
      'settings' => 'contact_code',
      'type' => 'textarea',
    )
  ));
}
add_action("customize_register", "numerica_contact_customize_register");


add_editor_style('style-editor.css');



// footer section
function numerica_footer_customize_register($wp_customize)
{
  $wp_customize->add_section("footer", array(
    "title" => __("Footer section", "numerica"),
    "priority" => 30,
  ));

  $wp_customize->add_setting("footer_code", array(
    "default" => "Footer section",
    "transport" => "postMessage",
    'sanitize_callback'  => 'esc_attr',
  ));

  $wp_customize->add_control(new WP_Customize_Control(
    $wp_customize,
    "footer_code",
    array(
      "label" => __('Enter the text for the footer section', 'numerica'),
      "section" => "footer",
      "settings" => "footer_code",
      "type" => "textarea",
    )
  ));
}
add_action("customize_register", "numerica_footer_customize_register");


// disable emojis
function numerica_disable_emojis()
{
  remove_action('wp_head', 'print_emoji_detection_script', 7);
  remove_action('admin_print_scripts', 'print_emoji_detection_script');
  remove_action('wp_print_styles', 'print_emoji_styles');
  remove_action('admin_print_styles', 'print_emoji_styles');
  remove_filter('the_content_feed', 'wp_staticize_emoji');
  remove_filter('comment_text_rss', 'wp_staticize_emoji');
  remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
  add_filter('tiny_mce_plugins', 'disable_emojis_tinymce');
  add_filter('wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2);
}
add_action('init', 'numerica_disable_emojis');


function disable_emojis_tinymce($plugins)
{
  if (is_array($plugins)) {
    return array_diff($plugins, array('wpemoji'));
  } else {
    return array();
  }
}


function disable_emojis_remove_dns_prefetch($urls, $relation_type)
{
  if ('dns-prefetch' == $relation_type) {
    $emoji_svg_url = apply_filters('emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/');

    $urls = array_diff($urls, array($emoji_svg_url));
  }
  return $urls;
}
