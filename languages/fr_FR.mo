��    7      �      �      �  "   �     �  
   �     �  	   �     �               2     J     `     p     �     �     �     �     �  &   �  %         <     ]     x     �     �     �     �     �     �     �     �     �                          ,     @     R  #   Z     ~     �     �     �     �     �  9   �  
   #  
   .     9     G  V   I  &   �     �  %   �       "   #     F     f     u     �     �     �     �     �     �     �  	   �  	   �     �     �          +  (   >  0   g     �     �     �     �     �     �     �     �     �     �     �          $     5     <     H     W     q     �  &   �     �     �     �     �     �     �  5   �  
        !     .     ?  D   A      �     �  %   �   A collection of full page layouts. A comment on &ldquo;%1$s&rdquo; Arrow icon Back To Home Checkmark Color nameAccent Color nameAccent Five Color nameAccent Four Color nameAccent Three Color nameAccent Two Color nameBase Color nameBase Two Color nameContrast Color nameContrast Three Color nameContrast Two Comments are closed. Contact section Enter the text for the contact section Enter the text for the footer section Font size nameExtra Extra Large Font size nameExtra Large Font size nameLarge Font size nameMedium Font size nameSmall Footer section Huge Large Next Numerica Oups... Page not found... Pages Patrice Andreani Pill Previous Primary menu Proudly powered by  Read More &raquo; Regular Simple, lightweight WordPress theme Small Space size nameExtra-small Space size nameLarge Space size nameMedium Space size nameSmall Theme There is an error, the requested page haven't been found. Title menu With arrow With asterisk Y comments title%1$s comments on &ldquo;%2$s&rdquo; %1$s comments on &ldquo;%2$s&rdquo; comments titleA comment %1$s comments https://andre-ani.fr https://gitlab.com/andre0ani/numerica Project-Id-Version: numerica
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-11-18 11:35+0100
PO-Revision-Date: 2023-12-31 04:23+0000
Last-Translator: Numericatous
Language-Team: Français
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Loco https://localise.biz/
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: _e;__;_nx;esc_html_e;esc_html;esc_html__
X-Poedit-SourceCharset: UTF-8
X-Loco-Version: 2.6.6; wp-6.4.2
X-Poedit-SearchPath-0: footer.php
X-Poedit-SearchPath-1: functions.php
X-Poedit-SearchPath-2: header.php
X-Poedit-SearchPath-3: index.php
X-Poedit-SearchPath-4: page.php
X-Poedit-SearchPath-5: page-single.php
X-Poedit-SearchPath-6: 404.php
X-Poedit-SearchPath-7: comments.php
 Collection de modèles pleine page Un commentaire sur « %1$s » Icône flèche Retour à l’accueil Case à cocher Accent Accent cinq Accent quatre Accent trois Accent deux Base Base deux Contraste Contraste trois Contraste deux Les commentaires sont fermés. Section de contact Saisissez le texte de la section contact Saisissez le texte de la section du pied de page Extra extra large Extra large Large Moyen Petit Section du pied de page Énorme Grand Suivant Numerica Aie… La page est introuvable Pages Patrice Andreani Pilule Précédent Menu principal Fièrement propulsé par  Lire la suite &raquo; Normal Thème simple et léger pour WordPress Petit Extra petit Large Moyen Petit Thème Il y a une erreur, la page demandée est introuvable. Menu titre Avec flèche Avec astérisque Y %1$s commentaire sur « %2$s » %1$s commentaires sur « %2$s » Un commentaire %1$s commentaires https://andre-ani.fr https://gitlab.com/andre0ani/numerica 