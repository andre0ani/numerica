<?php get_template_part('template-parts/header'); ?>
<!-- page -->
<?php if (is_front_page()) { ?>
	<div class="pure-g">
		<div class="pure-u-1-1 column">

			<div class="blog-post">
				<?php if (is_active_sidebar('widget-1')) : ?>
					<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
						<?php dynamic_sidebar('widget-1'); ?>
					</div>
				<?php endif; ?>
			</div>

		</div>
	</div>
<?php } ?>

<!-- show the page -->
<div class="pure-g">
	<div class="pure-u-3-4 pure-u-md-1-1 pure-u-lg-1-1 column page">

		<?php
		if (have_posts()) : while (have_posts()) : the_post();
				get_template_part('page-single', get_post_format());
			endwhile;
		endif;
		?>

	</div>
</div>

<?php if (is_front_page()) { ?>
	<div class="pure-g">
		<div class="pure-u-1-4 pure-u-md-1-1 pure-u-lg-1-1 column">
			<div class="blog-post">
				<?php if (is_active_sidebar('widget-2')) : ?>
					<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
						<?php dynamic_sidebar('widget-2'); ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
<?php } ?>

<?php get_template_part('template-parts/footer'); ?>